fn main() {
    let name = "Snow";
    let day_name = "Best Day";
    pretty_print(name);
    happy(day_name);
}

fn happy(day_name: &str) {
    println!("Today is the {}", day_name);
}

fn pretty_print(name: &str) {
    println!("Hello {}", name)
}